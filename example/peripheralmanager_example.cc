/*
 * Copyright (C) 2015 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <unistd.h>

#include <memory>

#include <base/at_exit.h>
#include <base/logging.h>
#include <base/message_loop/message_loop.h>
#include <base/sys_info.h>
#include <base/time/time.h>
#include <binderwrapper/binder_wrapper.h>
#include <brillo/flag_helper.h>
#include <peripheralmanager/peripheral_manager_client.h>

int main(int argc, char* argv[]) {
  brillo::FlagHelper::Init(argc, argv, "Example PeripheralManager client.");
  logging::InitLogging(logging::LoggingSettings());
  android::BinderWrapper::Create();

  android::PeripheralManagerClient client;
  CHECK(client.Init());

  LOG(INFO) << "AddInts " << client.AddInts(10, 20);
  LOG(INFO) << "Exiting";
  return 0;
}
